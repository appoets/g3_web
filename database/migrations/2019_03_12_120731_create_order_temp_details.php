<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTempDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_temp_details', function (Blueprint $table) {
            $table->increments('id');
            $table->json('user_address');
            $table->json('shop');
            $table->json('request_input');

            $table->string('quantity')->nullable();
            $table->string('gross')->nullable();
            $table->string('discount')->nullable();
            $table->string('wallet_amount')->nullable();
            $table->string('promocode_amount')->nullable();

            $table->string('restaurant_or_packing_charges')->nullable();
            $table->string('tax')->nullable();
            $table->string('net')->nullable();

            $table->string('payable')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_id')->nullable();

            $table->string('total_pay')->nullable();
            $table->string('ripple_price')->nullable();
            $table->string('DestinationTag')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
