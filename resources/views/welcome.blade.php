<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>{{Setting::get('site_title')}}</title>
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="{{ Setting::get('site_favicon', asset('favicon.ico')) }}">
      <!-- Bootstrap CSS -->
      <link href="{{ asset('assets/user/css/bootstrap.min.css')}}" rel="stylesheet">
      <!-- Font Awesome CSS -->
      <link href="{{ asset('assets/user/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

      <link href="{{ asset('assets/user/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
      <!-- Material Icons CSS -->
      <link href="{{ asset('assets/user/material-icons/css/materialdesignicons.min.css')}}" rel="stylesheet">
      <!-- Slick CSS -->
      <link href="{{ asset('assets/user/css/slick.css')}}" rel="stylesheet">
      <link href="{{ asset('assets/user/css/slick-theme.css')}}" rel="stylesheet">
      <link href="{{ asset('assets/user/css/owl.carousel.css')}}" rel="stylesheet">
     <!--  <link href="{{ asset('assets/user/css/font.css')}}" rel="stylesheet"> -->
      <link href="{{ asset('assets/user/css/style.css')}}" rel="stylesheet">
      <link href="{{ asset('assets/user/css/style-new.css')}}" rel="stylesheet">

   </head>
   <body>
   <div id="overlay">
<div class="spinner"></div> 

</div>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-2">
                    <div class="logo">
                        <img src="{{ Setting::get('site_logo', asset('assets/user/images/logo.png')) }}" alt="" style="width:140%;">
                    </div>

                </div>
                <div class="col-md-8 col-lg-8">
                    <nav>
                        <ul>
                           @if(Auth::guest())
                              <li><a href="javascript:void(0);" class="signinform"> login</a></li>
                              <li><a href="javascript:void(0);" class="signupform">sign up</a></li>
                            @endif
                            <!-- <li><a href="#">offers</a></li>
                            <li><a href="#">help</a></li> -->
                        </ul>
                    </nav>
                </div>
                <div class="col-md-2">
                    <div class="language-section">
                        <ul>
                            <li class="select-lang">en</li>
                            <li>se</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Banner -->
    <section class="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 banner-form">
                    <div class="from-banner">
                        <h1>Discover restaurant that deliver near you</h1>
                        <form action="{{url('restaurants')}}" id="my_map_form">
                            <ul>
                               <!--  <li>
                                    <select name="city" class="s-city">
                                        <option>Select City</option>
                                        <option>Västerås</option>
                                        <option>Enköping</option>
                                        <option>Stockholm</option>
                                    </select>
                                </li> -->
                                <li><input type="text" id="pac-input" name="search_loc" placeholder="Enter an area"
                                        class="area-select" required> <span class=" my_map_form_current search"><button>Show
                                            Restaurants</button></span> <span class=" my_map_form_current" style="cursor: pointer;"><i class="ion-pinpoint"></i></span></li>
                                <input type="hidden" id="latitude" name="latitude" value="{{ old('latitude') }}" readonly >
                                <input type="hidden" id="longitude" name="longitude" value="{{ old('longitude') }}" readonly >
                                <div id="my_map"   style="height:500px;width:500px;display: none" ></div>            

                            </ul>
                        </form>
                           <form  action="{{url('restaurants')}}" id="my_map_form_current" >
                                <input type="hidden" id="pac-input_cur" class="form-control search-loc-form" placeholder="Search for area,street name..." name="search_loc" value="{{ old('latitude') }}" >
                                <input type="hidden" id="latitude_cur" name="latitude" value="{{ old('latitude') }}" readonly >
                                <input type="hidden" id="longitude_cur" name="longitude" value="{{ old('longitude') }}" readonly >
                                
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end of banner-->

    <!-- find us in city-->
    <section class="find-city">
        <h2>Find Us In Your City</h2>
        <div class="container">
            <div class="row">


                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 city-images">
                    <img src="assets/user/images/city-1.jpg" alt="" class="img-responsive">
                    <p>Västerås</p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 city-images">
                    <img src="assets/user/images/city-2.jpg" alt="" class="img-responsive">
                    <p>Enköping</p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 city-images">
                    <img src="assets/user/images/city-3.jpg" alt="" class="img-responsive">
                    <p>Stockholm</p>
                </div>

            </div>
        </div>
        </div>

    </section>
    <section class="popular">
        <div class="container">
            <div class="row">
                <h2>Most Popular Restaurants</h2>
                <div class="carousel-wrap">
                        <div class="owl-carousel">
                          <div class="item"><img src="assets/user/images/res-1.jpg"></div>
                          <div class="item"><img src="assets/user/images/res-2.jpg"></div>
                          <div class="item"><img src="assets/user/images/res-3.jpg"></div>
                          <div class="item"><img src="assets/user/images/res-4.jpg"></div>
                          <div class="item"><img src="assets/user/images/res-5.jpg"></div>
                        </div>
                      </div>
            </div>
        </div>
    </section>
    <!-- End of find us in city-->

    <!-- APP Section-->
    <section class="app">
        <div class="container">
            <div class="row">
                <div class="col-md-6 app-mobile">
                <img src="assets/user/images/mobile-app.png" alt="">
                </div>
                <div class="col-md-6 app-text">
                    <h2>Download the app.Order with a tap</h2>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corrupti repellat ad dolores dignissimos veritatis dolore eligendi cupiditate explicabo officia mollitia? Quisquam dicta molestiae optio repellendus vitae itaque amet nihil aliquid.</p>
                    <a href="{{Setting::get('ANDROID_APP_LINK')}}" class="download-img"><img src="assets/user/images/google-play.png" alt=""></a>
                    <a href="{{Setting::get('IOS_APP_LINK')}}" class="download-img"><img src="assets/user/images/app-store.png" alt=""></a>
                </div>
            </div>
        </div>
    </section>
    <!-- End of APP Section-->
    <!-- Newsletter section-->
<section class="newsletter">
<h2>Subscribe to our newsletter</h2>
<h3>Receive deals/voucher from all our top restaurants via e-mail</h3>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="newsletter">
                <form action="">
                    <ul>
                        <li><input type="text" name="" id="" placeholder="City"></li>
                        <li><input type="email" name="" id="" placeholder="Email Address"></li>
                        <li><button>SUBSCRIBE</button></li>
                    </ul>
                </form>
            </div>
            <form action=""></form>
        </div>
    </div>
</div>
</section>
    <!-- End of newsletter section-->

</body>
<!-- Scripts-->

   <!-- Footer Starts -->
   @include('user.layouts.partials.footer')
   <!-- Footer Ends -->
   </div>
   <!-- Login Warapper Ends -->

 <!--  <script src="{{ asset('assets/user/js/scripts.js')}}"></script> -->
   <script src="{{ asset('assets/user/js/jquery.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('assets/user/js/bootstrap.min.js')}}"></script>
    <!-- Slick Slider JS -->
    <script src="{{ asset('assets/user/js/slick.min.js')}}"></script>
    <!-- Sidebar JS -->
    <script src="{{ asset('assets/user/js/asidebar.jquery.js')}}"></script>
    <!-- Map JS -->
 <!--    <script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('GOOGLE_MAP_KEY')}}"></script> -->
    <!-- <script src="{{ asset('assets/user/js/jquery.googlemap.js')}}"></script> -->
    <!-- Incrementing JS -->
    <script src="{{ asset('assets/user/js/incrementing.js')}}"></script>
    <!-- Scripts -->
    <script src="{{ asset('assets/user/js/scripts.js')}}"></script>
    @include('user.layouts.partials.script')
      <script src="{{ asset('assets/user/js/owl.carousel.js')}}"></script>
  
<script>
$('.owl-carousel').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: true,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
})
var overlay = document.getElementById("overlay");

window.addEventListener('load', function(){
  overlay.style.display = 'none';
})
</script>
   
  <!-- Start of LiveChat (www.livechatinc.com) code -->
         <!-- <script type="text/javascript">
              window.__lc = window.__lc || {};
              window.__lc.license = 8256261;
              (function() {
                  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
              })();
          </script> -->
          <!-- End of LiveChat code -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
             <!--  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113150309-2"></script>
              <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-113150309-2');
             </script>   -->
<!-- <script type="text/javascript">
    $(document).bind("contextmenu",function(e) {
 e.preventDefault();
});
$(document).keydown(function(e){
    console.log(e);
    if(e.which === 123){
       return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
    }
});
</script> -->
   </body>
</html>