<?php

use Illuminate\Database\Seeder;

class TransportersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transporters')->delete();
        DB::table('transporters')->insert([
            [
                'name' => 'Demo User',
                'email' => 'demo@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919898765654'
            ],
            [
                'name' => 'Rave Demo ',
                'email' => 'rave@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '919876542735'
            ],

            [

                'name' => 'Vish Demo ',
                'email' => 'vish@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+918527419630'
            ],

            [
            	'name' => 'Rav Demo ',
                'email' => 'rav@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919874562580'
            ],
             [
                'name' => 'Richi Demo ',
                'email' => 'richi@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917532145741'
            ],

            [

                'name' => 'Carrie Demo ',
                'email' => 'carrie@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+918521479630'
            ],

            [
            	'name' => 'Rike Demo ',
                'email' => 'rike@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917410852096'
            ],
            
             
            [

                'name' => 'Saar Demo ',
                'email' => 'saar@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917531597890'
            ],

            [
            	'name' => 'David Demo ',
                'email' => 'david@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917418529630'
            ],
             [
                'name' => 'Veer Demo ',
                'email' => 'veer@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919874561231'
            ],

            [

                'name' => 'Ramm Demo ',
                'email' => 'ramm@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561210'
            ],

            [
            	'name' => 'Dev Demo ',
                'email' => 'dev@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919410852096'
            ],
            [

                'name' => 'Ravi Demo ',
                'email' => 'ravi@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561238'
            ],

            [
            	'name' => 'Raj Demo ',
                'email' => 'raj@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561237'
            ],
            
             
            [

                'name' => 'Ram Demo ',
                'email' => 'ram@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561236'
            ],

            [
            	'name' => 'Nik Demo ',
                'email' => 'nik@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561235'
            ],
             [
                'name' => 'Jam Demo ',
                'email' => 'jam@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561234'
            ],

            [

                'name' => 'Mason Demo ',
                'email' => 'Mason@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561232'
            ],

            [
            	'name' => 'Noah Demo ',
                'email' => 'noah@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561231'
            ],
              [
            	'name' => 'Raja Demo ',
                'email' => 'raja@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+917894561230'
            ]

        ]);
    }
}
