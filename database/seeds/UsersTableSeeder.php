<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            [
                'name' => 'Demo User',
                'email' => 'demo@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+915454545454',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            
            [
                'name' => 'raja demo',
                'email' => 'raja@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543210',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'noah demo',
                'email' => 'noah@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919712345678',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],            
            [
                'name' => 'Mason demo',
                'email' => 'Mason@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919712345679',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'nik demo',
                'email' => 'nik@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919891234557',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'jam demo',
                'email' => 'jam@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919891234567',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],           
            [
                'name' => 'ram demo',
                'email' => 'ram@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919812345678',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'raj demo',
                'email' => 'raj@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919854321678',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'ravi demo',
                'email' => 'ravi@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543212',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'dev demo',
                'email' => 'dev@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543222',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'ramm demo',
                'email' => 'ramm@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543221',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'veer demo',
                'email' => 'veer@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543121',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'david demo',
                'email' => 'david@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543123',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'saar demo',
                'email' => 'saar@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543133',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'rike demo',
                'email' => 'rike@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543233',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'carrie demo',
                'email' => 'carrie@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876543234',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'richi demo',
                'email' => 'richi@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876541233',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'rav demo',
                'email' => 'rav@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876541733',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'vish demo',
                'email' => 'vish@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876541734',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ],
            [
                'name' => 'rave demo',
                'email' => 'rave@demo.com',
                'password' => bcrypt('123456'),
                'phone' => '+919876542735',
                'device_type' => 'ios',
                'login_by' => 'manual'
            ]
            
        ]);
    }
}
