<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_images')->delete();
        DB::table('product_images')->insert([
            [
                'product_id' => 1,
                'url' => asset('/storage/products/1.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 2,
                'url' => asset('/storage/products/2.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 3,
                'url' => asset('/storage/products/3.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 4,
                'url' => asset('/storage/products/4.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 5,
                'url' => asset('/storage/products/5.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 6,
                'url' => asset('/storage/products/6.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 7,
                'url' => asset('/storage/products/7.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 8,
                'url' => asset('/storage/products/8.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 9,
                'url' => asset('/storage/products/9.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 10,
                'url' => asset('/storage/products/10.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 11,
                'url' => asset('/storage/products/11.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 12,
                'url' => asset('/storage/products/12.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 13,
                'url' => asset('/storage/products/13.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 14,
                'url' => asset('/storage/products/14.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 15,
                'url' => asset('/storage/products/15.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 16,
                'url' => asset('/storage/products/16.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 17,
                'url' => asset('/storage/products/17.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 18,
                'url' => asset('/storage/products/18.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 19,
                'url' => asset('/storage/products/19.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 20,
                'url' => asset('/storage/products/20.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 21,
                'url' => asset('/storage/products/21.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 22,
                'url' => asset('/storage/products/22.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 23,
                'url' => asset('/storage/products/23.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 24,
                'url' => asset('/storage/products/24.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 25,
                'url' => asset('/storage/products/25.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 26,
                'url' => asset('/storage/products/26.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 27,
                'url' => asset('/storage/products/27.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 28,
                'url' => asset('/storage/products/28.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 29,
                'url' => asset('/storage/products/29.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 30,
                'url' => asset('/storage/products/30.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 31,
                'url' => asset('/storage/products/31.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 32,
                'url' => asset('/storage/products/32.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 33,
                'url' => asset('/storage/products/23.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 34,
                'url' => asset('/storage/products/34.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 35,
                'url' => asset('/storage/products/35.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 36,
                'url' => asset('/storage/products/36.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 37,
                'url' => asset('/storage/products/37.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 38,
                'url' => asset('/storage/products/38.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 39,
                'url' => asset('/storage/products/39.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 40,
                'url' => asset('/storage/products/40.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 41,
                'url' => asset('/storage/products/41.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 42,
                'url' => asset('/storage/products/42.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 43,
                'url' => asset('/storage/products/43.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 44,
                'url' => asset('/storage/products/44.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 45,
                'url' => asset('/storage/products/45.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 46,
                'url' => asset('/storage/products/46.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 47,
                'url' => asset('/storage/products/47.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 48,
                'url' => asset('/storage/products/48.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 49,
                'url' => asset('/storage/products/49.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 50,
                'url' => asset('/storage/products/50.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 51,
                'url' => asset('/storage/products/51.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 52,
                'url' => asset('/storage/products/52.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 53,
                'url' => asset('/storage/products/53.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 54,
                'url' => asset('/storage/products/54.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 55,
                'url' => asset('/storage/products/55.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 56,
                'url' => asset('/storage/products/56.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 57,
                'url' => asset('/storage/products/57.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 58,
                'url' => asset('/storage/products/58.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 59,
                'url' => asset('/storage/products/59.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 60,
                'url' => asset('/storage/products/60.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 61,
                'url' => asset('/storage/products/61.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 62,
                'url' => asset('/storage/products/62.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 63,
                'url' => asset('/storage/products/63.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 64,
                'url' => asset('/storage/products/64.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 65,
                'url' => asset('/storage/products/65.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 66,
                'url' => asset('/storage/products/66.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 67,
                'url' => asset('/storage/products/67.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 68,
                'url' => asset('/storage/products/68.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 69,
                'url' => asset('/storage/products/69.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 70,
                'url' => asset('/storage/products/70.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 71,
                'url' => asset('/storage/products/71.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 72,
                'url' => asset('/storage/products/72.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 73,
                'url' => asset('/storage/products/73.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 74,
                'url' => asset('/storage/products/74.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 75,
                'url' => asset('/storage/products/75.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 76,
                'url' => asset('/storage/products/76.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 77,
                'url' => asset('/storage/products/77.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 78,
                'url' => asset('/storage/products/78.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 79,
                'url' => asset('/storage/products/79.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 80,
                'url' => asset('/storage/products/80.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 81,
                'url' => asset('/storage/products/81.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 82,
                'url' => asset('/storage/products/82.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 83,
                'url' => asset('/storage/products/83.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 84,
                'url' => asset('/storage/products/84.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 85,
                'url' => asset('/storage/products/85.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 86,
                'url' => asset('/storage/products/86.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 87,
                'url' => asset('/storage/products/87.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 88,
                'url' => asset('/storage/products/88.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 89,
                'url' => asset('/storage/products/89.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 90,
                'url' => asset('/storage/products/90.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 91,
                'url' => asset('/storage/products/91.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 92,
                'url' => asset('/storage/products/92.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 93,
                'url' => asset('/storage/products/93.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 94,
                'url' => asset('/storage/products/94.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 95,
                'url' => asset('/storage/products/95.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 96,
                'url' => asset('/storage/products/46.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 97,
                'url' => asset('/storage/products/97.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 98,
                'url' => asset('/storage/products/98.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 99,
                'url' => asset('/storage/products/99.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 100,
                'url' => asset('/storage/products/100.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 1,
                'url' => asset('/storage/products/1A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 6,
                'url' => asset('/storage/products/6A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 12,
                'url' => asset('/storage/products/12A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 17,
                'url' => asset('/storage/products/17A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 23,
                'url' => asset('/storage/products/23A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 27,
                'url' => asset('/storage/products/27A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 33,
                'url' => asset('/storage/products/33A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 37,
                'url' => asset('/storage/products/37A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 44,
                'url' => asset('/storage/products/44A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 49,
                'url' => asset('/storage/products/49A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 53,
                'url' => asset('/storage/products/53A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 57,
                'url' => asset('/storage/products/57A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 62,
                'url' => asset('/storage/products/62A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 67,
                'url' => asset('/storage/products/67A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 71,
                'url' => asset('/storage/products/71A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 78,
                'url' => asset('/storage/products/78A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 81,
                'url' => asset('/storage/products/81A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 85,
                'url' => asset('/storage/products/85A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 91,
                'url' => asset('/storage/products/91A.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 97,
                'url' => asset('/storage/products/97A.jpg'),
                'position' => 1
            ],
        ]);        
    }
}
