<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('categories')->delete();
        DB::table('categories')->insert([
            [
                'name' => 'Burger',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Snacks',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Beverages',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Burgers',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Rice meals ',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Rice bowls',
                'shop_id' => 2,
                'description' => 'special'
            ],
             [
                'name' => 'Breakfast subs',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Salads',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Cookies and chips',
                'shop_id' => 3,
                'description' => 'special'
            ],
             [
                'name' => 'soup',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Starters',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Main course',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'soup',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Starters',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Main course',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Indian',
                'shop_id' => 6,
                'description' => 'special'
            ],
            [
                'name' => 'Chinese',
                'shop_id' => 6,
                'description' => 'special'
            ],
            [
                'name' => 'Arabian',
                'shop_id' => 6,
                'description' => 'special'
            ],
            [
                'name' => 'Lassi',
                'shop_id' => 7,
                'description' => 'special'
            ],
            [
                'name' => 'Juices',
                'shop_id' => 7,
                'description' => 'special'
            ],
            [
                'name' => 'Ice Creams',
                'shop_id' => 7,
                'description' => 'special'
            ],
            [
                'name' => 'Indian',
                'shop_id' => 8,
                'description' => 'special'
            ],
            [
                'name' => 'Chinese',
                'shop_id' => 8,
                'description' => 'special'
            ],
            [
                'name' => 'Arabian',
                'shop_id' => 8,
                'description' => 'special'
            ],
            [
                'name' => 'Maggi',
                'shop_id' => 9,
                'description' => 'special'
            ],
            [
                'name' => 'Platters',
                'shop_id' => 9,
                'description' => 'special'
            ],
            [
                'name' => 'Pastas',
                'shop_id' => 9,
                'description' => 'special'
            ],
            [
                'name' => 'Veg Pan Pizzas',
                'shop_id' => 10,
                'description' => 'special'
            ],
            [
                'name' => 'Non Veg Pan Pizzas',
                'shop_id' => 10,
                'description' => 'special'
            ],
            [
                'name' => 'Beverages',
                'shop_id' => 10,
                'description' => 'special'
            ]

         ]);
    }
}
