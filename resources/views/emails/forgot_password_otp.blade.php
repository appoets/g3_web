<div style="font-family: Avenir,Helvetica,sans-serif;box-sizing: border-box;padding: 25px 0;text-align: center;">
    Get Eat
</div>
<div style="font-family: Avenir,Helvetica,sans-serif;
    box-sizing: border-box;
    padding: 35px;">
    <h1 style="
    font-family: Avenir,Helvetica,sans-serif;
    box-sizing: border-box;
    color: #2f3133;
    font-size: 19px;
    font-weight: bold;
    margin-top: 0;
    text-align: left;">hello!</h1>
    <p style=" font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">You are receiving this email because we received a password reset request for your account.</p>
    @if($type=='user')
    <a style=" text-align: left;font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1"  >{{$data['otp']}}</a><br/>
    @elseif($type=='admin')
    <a style=" text-align: left;font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1" >{{$data['otp']}}</a><br/>
    @elseif($type=='shop')
    <a style="text-align: left;font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1" style="color:red">{{$data['otp']}}</a><br/>
    @endif
    <br/>
    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">If you did not request a password reset, no further action is required.</p>
    
    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
    Regards,<br/><br/>
    Admin<br/>
    geteat.in<br/>
    </p>
</div>