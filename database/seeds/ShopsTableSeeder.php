<?php

use Illuminate\Database\Seeder;
use App\Shop;
class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->delete();
        DB::table('shops')->insert([
            [
                'name' => 'kfc',
                'email' => 'kfc@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Triplicane, Chennai, Tamil Nadu, India',
                'maps_address' => 'Triplicane, Chennai, Tamil Nadu, India',
                'latitude' => '13.05871070',
                'longitude' => '80.27570630',
                'estimated_delivery_time' => '30',
                'phone' => '8765654345',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/1.jpeg'),
                'default_banner' => asset('/storage/shops/1a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Mcdonald',
                'email' => 'mcd@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Greams road, Chennai, Tamil Nadu, India',
                'maps_address' => 'Greams road, Chennai, Tamil Nadu, India',
                'latitude' => '13.060499',
                'longitude' => '80.254417',
                'estimated_delivery_time' => '30',
                'phone' => '8745645678',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/2.jpeg'),
                'default_banner' => asset('/storage/shops/2a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Subway',
                'email' => 'subway@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Kodambakkam, Chennai, Tamil Nadu, India',
                'maps_address' => 'Kodambakkam, Chennai, Tamil Nadu, India',
                'latitude' => '13.0551',
                'longitude' => '80.2221',
                'estimated_delivery_time' => '30',
                'phone' => '9809354653',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/3.jpeg'),
                'default_banner' => asset('/storage/shops/3a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Thalapakatti',
                'email' => 'Thalapakatti@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Nungambakkam, Chennai, Tamil Nadu, India',
                'maps_address' => 'Kodambakkam, Chennai, Tamil Nadu, India',
                'latitude' => '13.0595',
                'longitude' => '80.2425',
                'estimated_delivery_time' => '30',
                'phone' => '7645637465',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/4.jpeg'),
                'default_banner' => asset('/storage/shops/4a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Firdhouse',
                'email' => 'Firdhouse@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Thousand lights, Chennai, Tamil Nadu, India',
                'maps_address' => 'Thousand lights, Chennai, Tamil Nadu, India',
                'latitude' => '13.0590',
                'longitude' => '80.2542',
                'estimated_delivery_time' => '30',
                'phone' => '8967584657',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/5.jpeg'),
                'default_banner' => asset('/storage/shops/5a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Grill house',
                'email' => 'grillhouse@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Triplicane, Chennai, Tamil Nadu, India',
                'maps_address' => 'Triplicane, Chennai, Tamil Nadu, India',
                'latitude' => '13.05871070',
                'longitude' => '80.27570630',
                'estimated_delivery_time' => '30',
                'phone' => '8765654345',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/6.jpeg'),
                'default_banner' => asset('/storage/shops/6a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Lassi shop',
                'email' => 'Lassi@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Greams road, Chennai, Tamil Nadu, India',
                'maps_address' => 'Greams road, Chennai, Tamil Nadu, India',
                'latitude' => '13.060499',
                'longitude' => '80.254417',
                'estimated_delivery_time' => '30',
                'phone' => '8768884334',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/7.jpeg'),
                'default_banner' => asset('/storage/shops/7a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Zaitoon',
                'email' => 'zaitoon@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Kodambakkam, Chennai, Tamil Nadu, India',
                'maps_address' => 'Kodambakkam, Chennai, Tamil Nadu, India',
                'latitude' => '13.0551',
                'longitude' => '80.2221',
                'estimated_delivery_time' => '30',
                'phone' => '8768889345',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/8.jpeg'),
                'default_banner' => asset('/storage/shops/8a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Maggevala',
                'email' => 'Maggevala@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Nungambakkam, Chennai, Tamil Nadu, India',
                'maps_address' => 'Kodambakkam, Chennai, Tamil Nadu, India',
                'latitude' => '13.0595',
                'longitude' => '80.2425',
                'estimated_delivery_time' => '30',
                'phone' => '8768854345',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/9.jpeg'),
                'default_banner' => asset('/storage/shops/9a.jpeg'),
                'status' => 'active'
            ],
            [
                'name' => 'Pizza hut',
                'email' => 'pizza@demo.com',
                'password' => bcrypt('123456'),
                'address' => 'Thousand lights, Chennai, Tamil Nadu, India',
                'maps_address' => 'Thousand lights, Chennai, Tamil Nadu, India',
                'latitude' => '13.0590',
                'longitude' => '80.2542',
                'estimated_delivery_time' => '30',
                'phone' => '8760884345',
                'description' => 'good resturant',
                'avatar' => asset('/storage/shops/10.jpeg'),
                'default_banner' => asset('/storage/shops/10a.jpeg'),
                'status' => 'active'
            ]
        ]);       
        
    }
}
