@extends('admin.layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">@lang('menu.admin.disclaimer') Page</h3>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">

            <form role="form" method="POST" action="{{ route('admin.disclaimer_update') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="page" value="disclaimer">
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image">Document</label>

                            <input type="file" accept="*" name="image[]" class="dropify" id="image" aria-describedby="fileHelp" multiple data-default-file="{{ $disclaimer_document_url }}">

                            <!-- <input type="file" accept="image/*" required name="image[]" class="dropify form-control" id="image" multiple="" aria-describedby="fileHelp"> -->

                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                    </div>
                    
                </div>

                <div class="col-xs-12 mb-2">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/admin/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/dropify/dist/js/dropify.min.js') }}"></script>
<script type="text/javascript">
    $('.dropify').dropify();
</script>
@endsection