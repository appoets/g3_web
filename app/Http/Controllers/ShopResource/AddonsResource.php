<?php

namespace App\Http\Controllers\ShopResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Route;
use Exception;
use App\Addon;
use App\Http\Controllers\Resource\AddonsResource AS AdminAddonsResource;

class AddonsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
         
        $Addons = Addon::where('shop_id',$request->user()->id)->get();

        if($request->ajax()){
            return $Addons ;
        }
        return view(Route::currentRouteName(), compact('Addons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(Route::currentRouteName());
    }

    public function store(Request $request)
    {
        return (new AdminAddonsResource())->store($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $Addon = Addon::findOrFail($id);
            return view(Route::currentRouteName(), compact('Addon'));
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.shops.index')->with('flash_error', 'Shop not found!');
            return back()->with('flash_error', 'Addon not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.shops.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        return (new AdminAddonsResource())->update($request, $id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
         try {
            $Addon = Addon::with('addonproduct')->findOrFail($id);
            if($Addon->addonproduct->count() == 0){
               $Addon->delete(); 
           }else{
                if($request->ajax()){
                    return response()->json(['error' => trans('inventory.addons.not_remove')], 500);
                }
                return back()->with('flash_error', trans('inventory.addons.not_remove'));
           }
            if($request->ajax()){
                return Addon::all() ;
            }
            return back()->with('flash_success', trans('inventory.addons.remove'));
        } catch (ModelNotFoundException $e) {
            if($request->ajax()){
                return response()->json(['error' => $e->getMessage()], 500);
            }
            // return redirect()->route('admin.shops.index')->with('flash_error', 'Shop not found!');
            return back()->with('flash_error', trans('form.not_found'));
        } catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => $e->getMessage()], 500);
            }
            return back()->with('flash_error', trans('form.whoops'));
        }
    }
}
