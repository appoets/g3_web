<?php

use Illuminate\Database\Seeder;
use App\Product;
class ShopsBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_banners')->delete();
        DB::table('shop_banners')->insert([
            [
                'shop_id' => 1,
                'product_id' => 1,
                'url' => asset('/storage/shops/banner/1a.jpeg'),
                'position' => 1,
                'status' => 'active'
            ],
            [
                'shop_id' => 2,
                'product_id' => 11,
                'url' => asset('/storage/shops/banner/2a.jpeg'),
                'position' => 2,
                'status' => 'active'
            ],
            [
                'shop_id' => 3,
                'product_id' => 21,
                'url' => asset('/storage/shops/banner/3a.jpeg'),
                'position' => 3,
                'status' => 'active'
            ],
            [
                'shop_id' => 4,
                'product_id' => 31,
                'url' => asset('/storage/shops/banner/4a.jpeg'),
                'position' => 4,
                'status' => 'active'
            ],
            [
                'shop_id' => 5,
                'product_id' => 41,
                'url' => asset('/storage/shops/banner/5a.jpeg'),
                'position' => 5,
                'status' => 'active'
            ],
            [
                'shop_id' => 6,
                'product_id' => 51,
                'url' => asset('/storage/shops/banner/6a.jpeg'),
                'position' => 6,
                'status' => 'active'
            ],
            [
                'shop_id' => 7,
                'product_id' => 61,
                'url' => asset('/storage/shops/banner/7a.jpeg'),
                'position' => 7,
                'status' => 'active'
            ],
            [
                'shop_id' => 8,
                'product_id' => 71,
                'url' => asset('/storage/shops/banner/8a.jpeg'),
                'position' => 8,
                'status' => 'active'
            ],
            [
                'shop_id' => 9,
                'product_id' => 81,
                'url' => asset('/storage/shops/banner/9a.jpeg'),
                'position' => 9,
                'status' => 'active'
            ],
            [
                'shop_id' => 10,
                'product_id' => 91,
                'url' => asset('/storage/shops/banner/10a.jpeg'),
                'position' => 10,
                'status' => 'active'
            ]

            
        ]);        
    }
}
