<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        DB::table('products')->insert([
            [
                'name' => 'Veg zinger',
                'shop_id' => 1,
                'description' => 'special',                
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Chicken zinger',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0               
            ],
            [
                'name' => 'Veg longer',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Large pocorn',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Hot wings',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Smokey grilled',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 2
            ],
            [
                'name' => 'Veg strips',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Pepsi',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Coke',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Sprite',
                'shop_id' => 1,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Big mac',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Maharaja mac',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Mcspicy',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Spicy ricy bowl',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Cheesy ricy bowl',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken strips',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken McNuggets',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Chicken bowl',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken 65 strips',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken 65 McNuggets',
                'shop_id' => 2,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Western egg',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Egg and cheese sub',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Veggie delight salad',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Mexican patty salad',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Kebab',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'paneer tikka',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chocolate cookie',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1

            ],
            [
                'name' => 'lays and onion chips',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Magic masala chips',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Raisin cookies',
                'shop_id' => 3,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken soup',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Mutton soup',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Veg soup',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Grill chicken',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Tandoori chicken',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Prawn fry',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'fish fry',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Chicken biriyani',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Mutton biriyani',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Egg biriyani',
                'shop_id' => 4,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken soup',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Mutton soup',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Veg soup',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Grill chicken',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Tandoori chicken',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Prawn fry',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'fish fry',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken biriyani',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Mutton biriyani',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Egg biriyani',
                'shop_id' => 5,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Tawa Fish',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Prawn',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Paneer Tikka',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Egg Fried Rice',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Fried Rice',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Veg Fried Rice',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Noodles',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Chicken Wrap',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Chicken Shawarma',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Mexican Shawarma',
                'shop_id' => 6,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Sweet Lassi',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Salt Lassi',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Mango Lassi',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Fresh Lime',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Watermelon',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Blue Lime',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Mango Juice',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Buterscotch',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Vanilla',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Stawberry',
                'shop_id' => 7,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0              
            ],
            [
                'name' => 'Tawa Fish',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Prawn',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Paneer Tikka',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Egg Fried Rice',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Fried Rice',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Veg Fried Rice',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Noodles',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Wrap',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
             [
                'name' => 'Chicken Shawarma',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Mexican Shawarma',
                'shop_id' => 8,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0              
            ],
            [
                'name' => 'Paneer Maggi',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Veg Maggi',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Cheese Masala Maggi',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Maggi Salsa',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Mexican Platter',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Curry Platter',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Tangy Penne Pasta',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Arrabbiata Penne Pasta',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Cheese Pasta',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Fusion Pasta',
                'shop_id' => 9,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0                
            ],
            [
                'name' => 'Margherita Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Paneer Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Corn Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Spiced Chicken Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Exotica Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Chicken Italiano Pizza',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 1,
                'featured_position' => 1
            ],
            [
                'name' => 'Pepsi',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
             [
                'name' => 'Coke',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ],
            [
                'name' => 'Sprite',
                'shop_id' => 10,
                'description' => 'special',
                'featured' => 0,
                'featured_position' => 0
            ]
        ]);
        
    }
}
