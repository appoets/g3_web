@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive" style="overflow-x: hidden;">
            <form role="form" method="POST" action="{{ route('admin.reason_update', $Reason->id) }}">
                {{ csrf_field() }}
                <h4 class="m-t-0 header-title">
                    <b>Edit Reason</b>
                </h4>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                            <label for="reason">@lang('menu.admin.reason')</label>

                            <input id="reason" type="text" class="form-control" name="reason" value="{{ old('reason', $Reason->reason) }}" required autofocus>

                            @if ($errors->has('reason'))
                            <span class="help-block">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 mb-2">
                    <a href="{{ route('admin.reasons') }}" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection