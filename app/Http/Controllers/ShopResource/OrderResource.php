<?php

namespace App\Http\Controllers\ShopResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Route;
use Exception;
use Setting;

use App\Order;
use App\Transporter;
use App\OrderInvoice;
use App\OrderRating;
use App\TransporterShift;
use App\Usercart;
use App\OrderTiming;
use Auth;
use App\Http\Controllers\SendPushNotification;
use Carbon\Carbon;
use App\Shop;
use App\UserAddress;
use App\CancelReason;
class OrderResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $reason_list = CancelReason::get();
        if($request->get('list')) { 
            $Providers = Transporter::pluck('name','id');
          /* if($request->has('all')){ 
                $Order = Order::where('status','!=','COMPLETED');
                
            }else{
                $Order = Order::where('status','COMPLETED');
            }*/
            $Order = Order::where('shop_id',Auth::user()->id);
           
           
            if($request->has('status') && $request->status != 'ALL'){
                $Order->where('status',$request->status);
            }

            if($request->has('dp')){
                $Order->where('transporter_id',$request->dp);
            }

            if($request->has('start_date') && $request->has('end_date')){
                 
                $Order->whereBetween('created_at',[Carbon::parse($request->start_date),Carbon::parse($request->end_date)]);
                
            }
            else if($request->has('start_date')){
                 
                $Order->whereBetween('created_at',[Carbon::parse($request->date),Carbon::parse($request->date)->addDay()]);
            }
            else if($request->has('end_date')){
                 
                $Order->whereBetween('created_at',[Carbon::parse($request->date),Carbon::parse($request->date)->addDay()]);
            }
           $Orders = $Order->get();
           if($request->has('status') && $request->status != 'ALL'){
              $tot_incom_resp = Order::where('shop_id',Auth::user()->id)->where('status',$request->status)->count();
             }else{
                $tot_incom_resp = Order::where('shop_id',Auth::user()->id)->count();
             }

           if($request->ajax()){ 
               return [
                   'Orders' => $Orders,
                   'tot_incom_resp' => $tot_incom_resp
               ];
           }
            
            
            return view(Route::currentRouteName().'-list', compact('Orders','Providers','reason_list'));
        }
        if($request->get('order_id')) {
            $Orders = Order::where('id',$request->get('order_id'))->where('shop_id',Auth::user('shop')->id)->get();
            if($request->ajax()){ 
                if($request->has('q')){
                    return $Orders;
                }
            }
            return view(Route::currentRouteName().'-search', compact('Orders','reason_list'));
        }
        if($request->t=='ordered'){
            $Orders = Order::where('shop_id',Auth::user()->id)->whereIN('orders.status',['ORDERED'])
                ->orderBy('id','DESC')->get();;

        }else
        if($request->t=='processing'){
            $Orders = Order::where('shop_id',Auth::user()->id)->whereIN('orders.status',['RECEIVED',
                'PROCESSING',
                'ASSIGNED',
                'SEARCHING',
                'REACHED',
                'PICKEDUP',
                'ARRIVED',
                ])
                ->orderBy('id','DESC')->get();;

        }else
        if($request->t=='pending'){
            if($request->has('delivery_date')){ 
                $cur_date = \Carbon\Carbon::parse($request->delivery_date);
                $last_date = \Carbon\Carbon::parse($request->delivery_date)->addDay();
            $Orders = Order::where('shop_id',Auth::user()->id)->whereIN('orders.status',['ORDERED','RECEIVED'])
                ->whereBetween('delivery_date',[$cur_date,$last_date])->orderBy('id','DESC')->get();;
            }else{
            $Orders = Order::where('shop_id',Auth::user()->id)->whereIN('status',['ORDERED','RECEIVED'])->orderBy('id','DESC')->get();
            }

        } else if ($request->t=='accepted') {
            $Orders = Order::where('shop_id',Auth::user()->id)->whereIN('status', ['PROCESSING','ASSIGNED','READY'])->get();
        } else if ($request->t=='ongoing') {
            $Orders = Order::where('shop_id',Auth::user()->id)->whereIn('status', [
                'REACHED',
                'PICKEDUP',
                'ARRIVED',
            ])->get();
        } else if ($request->t=='cancelled') {
            $Orders = Order::where('shop_id',Auth::user()->id)->where('status', 'CANCELLED')->get();
        }else{
            $Orders = Order::where('shop_id',Auth::user()->id)->where('status', 'COMPLETED')->get();
        }
        $tot_incom_resp = Order::where('shop_id',Auth::user()->id)->where('status','ORDERED')->count();
        if($request->ajax()){ 
            return [
                'Orders' => $Orders,
                'tot_incom_resp' => $tot_incom_resp
            ];
        }
        return view(Route::currentRouteName(), compact('Orders','tot_incom_resp','reason_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(Route::currentRouteName());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $Order = Order::findOrFail($id);
            // dd($request->route()->getPrefix());

            $Transporters = Transporter::where('status','online')->get();
            $Carts= Usercart::with('product','product.prices','product.images','cart_addons')->where('order_id',$id)->withTrashed()->get();
            if($request->ajax()){
                return [
                'Order' => $Order,
                'Cart' => $Carts
                ];
            }
            return view(Route::currentRouteName(), compact('Order', 'Transporters'));
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.orders.index')->with('flash_error', 'Order not found!');
            return back()->with('flash_error', 'Order not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.orders.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(Route::currentRouteName());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_method(Request $request)
    { 
        $this->validate($request, [
                //'transporter_id' => 'required_without:status|exists:transporters,id',
                'status' => 'required|in:REACHED,PICKEDUP,ARRIVED,COMPLETED,RECEIVED,CANCELLED',
            ]);
        if($request->status=='RECEIVED'){
            $this->validate($request, [
                'order_ready_time' => 'required'   
            ]);
        }
        try {
            $Order = Order::findOrFail($request->id);
            $user = UserAddress::where('user_id', $Order->user_id)->orderBy('id','DESC')->first();
            $shop_details = Shop::findOrFail(Auth::user()->id);
            if($request->status=='RECEIVED'){
                $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$shop_details->latitude.",".$shop_details->longitude."&destination=".$user->latitude.",".$user->longitude."&mode=driving&key=".Setting::get('GOOGLE_MAP_KEY');

                $json = curl($details);
                
                $details = json_decode($json, TRUE);
                $route_key = $details['routes'][0]['legs'];

                $distance = $route_key[0]['distance']['text'];
                $distance_time = $route_key[0]['duration']['text'];
                $int_distance_time = (int) filter_var($distance_time, FILTER_SANITIZE_NUMBER_INT);
                $int_order_ready_time = (int) filter_var($request->order_ready_time, FILTER_SANITIZE_NUMBER_INT);
                $Order->eta = (int)$int_order_ready_time+(int)$int_distance_time;
                $Order->order_ready_time = $request->order_ready_time;
            }
            $Order->status = $request->status;
           
            $Order->save();
            OrderTiming::create([
                    'order_id' => $Order->id,
                    'status' => $Order->status
            ]);
            
            $order = Order::find($Order->id);
            $order->admin = \App\Admin::find(1);
            $shop_details = Shop::where('id', Auth::user()->id)->first();
            if($request->status=='REACHED'){ }
            if($request->status=='PICKEDUP'){ }
            if($request->status=='ARRIVED'){ }
            if($request->status=='COMPLETED'){ }

            if($request->status=='RECEIVED'){
                $push_message = trans('order.order_accept_shop').' '.$shop_details->name;
                (new SendPushNotification)->sendPushToUser($Order->user_id,$push_message,$page =['page'=>'userorder','status'=>$Order->status,'dispute_status'=>$Order->dispute]);
                //site_sendmail($order,'order_status','Order Confirmed','order');
            }
            if($request->status=='CANCELLED'){
                $Order->cancel_reason_id = $request->cancel_reason_id;
                $Order->save();
                $push_message = $shop_details->name.' '.trans('order.order_canceled_by_shop');
                (new SendPushNotification)->sendPushToUser($Order->user_id,$push_message,$page =['page'=>'userorder','status'=>$Order->status,'dispute_status'=>$Order->dispute]);
                //site_sendmail($order,'order_status','Order Cancelled','order');
                //site_sendmail($order,'order_status','Order Cancelled','order_admin');
            }
            if($request->ajax()){
                return $Order;
            }
            // return redirect()->route('admin.orders.show', $Order->id);
            return back();
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.orders.index')->with('flash_error', 'Order not found!');
            return back()->with('flash_error', 'Order not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.orders.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function totalIncoming(Request $request)
    {
        try {
            $Orders = $request->user()->orders->where('status','ORDERED')->count();
            return $Orders;
        } catch (ModelNotFoundException $e) {
            
            return 0;
        } catch (Exception $e) {
            return 0;
        }
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    {
        try {
            $Orders_complete = Order::where('shop_id',Auth::user()->id)->whereIN('status',['COMPLETED']);
            $Orders_cancell = Order::where('shop_id',Auth::user()->id)->whereIN('status',['CANCELLED']);
            if($request->has('dp')){
                $Orders_complete->where('transporter_id',$request->dp);
                $Orders_cancell->where('transporter_id',$request->dp);
            }
            if($request->has('start_date') && $request->has('end_date')){
                 
                $Orders_complete->whereBetween('created_at',[Carbon::parse($request->start_date),Carbon::parse($request->end_date)]);
                $Orders_cancell->whereBetween('created_at',[Carbon::parse($request->start_date),Carbon::parse($request->end_date)]);
                
            }
            else if($request->has('start_date')){
                 
                $Orders_complete->whereBetween('created_at',[Carbon::parse($request->date),Carbon::parse($request->date)->addDay()]);
                $Orders_cancell->whereBetween('created_at',[Carbon::parse($request->date),Carbon::parse($request->date)->addDay()]);
            }
            else if($request->has('end_date')){
                 
                $Orders_complete->whereBetween('created_at',[Carbon::parse($request->date),Carbon::parse($request->date)->addDay()]);
                $Orders_cancell->whereBetween('created_at',[Carbon::parse($request->date),Carbon::parse($request->date)->addDay()]);
            }
            $Orders_complete_res = $Orders_complete->get();
            $Orders_cancell_res = $Orders_cancell->get();
            return [
                'COMPLETED' => $Orders_complete_res,
                'CANCELLED' => $Orders_cancell_res
            ];
        } catch (ModelNotFoundException $e) {
            
            return [];
        } catch (Exception $e) {
            return [];
        }
    }

    public function transporterlist(Request $request){
        try {
            $Orders_complete = Transporter::all();
            
            return $Orders_complete;
        } catch (ModelNotFoundException $e) {
            
            return [];
        } catch (Exception $e) {
            return [];
        }
    }
}
